# RT1 - 12/12/2023

Sur l'accueil d'un projet github ou gitlab vous trouverez les liens pour cloner les codes sources. Il y a un lien "https" (nécessitant identifiant et mot de passe) et un lien "ssh" (nécessitant la clé et les configurations effectuées ci-dessous). 
Une liaison en ssh permet d'entrer une seule fois un mot de passe (la passphrase de votre clé) lorsque la clé est ajoutée à votre agent ssh de votre terminal.
Le lien https demande d'entrer l'identifiant et le mot de passe de votre compte gitlab ou github à chaque fois qu'une action communique avec le répertoire distant.

Vous aurez plus d'information dans les documentations officielles. Ici, je résume pour vous.

La documentation officielle de gitlab.com :

[https://docs.gitlab.com/ee/user/ssh.html](https://docs.gitlab.com/ee/user/ssh.html)

La documentation officielle de github.com :

[https://docs.github.com/en/authentication/connecting-to-github-with-ssh](https://docs.github.com/en/authentication/connecting-to-github-with-ssh)


## 1) Prérequis

- Avoir un compte gitlab ou github.
- Pour Windows, avoir installé git (on utilise l'outil GIT BASH)

### Configuration à faire une seul fois sur votre machine personnelle

À faire une seule fois si vous n'avez pas configuré vos informations en globale.

```sh
git config --global user.name "nom d'utilsateur"
git config --global user.email "exemplemail@etudiant.re"
```

## 2) Pair de clé publique / clé privé

Les commandes sont à exécuter dans un terminal type Bash (ou GIT BASH sous windows).

Générer votre clé ssh avec la commande suivante :

/!\ Modifier le nom du fichier par un autre qui vous convient (exemple keyName ou gitkey)

```sh
ssh-keygen -t ed25519 -C "gitkey"
```

Vous obtenez deux fichiers une clé privé (keyName) et une clé publique (keyName.pub)

--> Le contenu de la clé publique est à copier dans votre compte dans la partie "ssh key"
```
cat ~/.ssh/keyName.pub
```

## 3) Ajouter la clé privé au terminal

Activer votre agent ssh dans le terminal :
```
eval $(ssh-agent -s)
```

Ajout de clé privé à l'agent ssh :
```
ssh-add ~/.ssh/keyName
```

## 4) Définir la clé préférée pour l'authentification

Modifier ou créer votre fichier ~/.ssh/config :

```sh
nano ~/.ssh/config  
```

Ajouter les lignes suivantes :
```
Host gitlab.com
        PreferredAuthentications publickey
        IdentityFile ~/.ssh/keyName
```

### Différence pour github.com 

La connexion sur le port 22 peut-être bloqué. Il faut spécifier le port 443 pour le Hostname ssh.github.com

[https://gist.github.com/Tamal/1cc77f88ef3e900aeae65f0e5e504794
](https://gist.github.com/Tamal/1cc77f88ef3e900aeae65f0e5e504794)

```
Host github.com
        PreferredAuthentications publickey
        IdentityFile ~/.ssh/keyName
        Hostname ssh.github.com
        Port 443
```

## 5) Cloner le projet en local

**Prérequis avoir un projet gitlab existant**

changer de répertoire de travail (adapter la commande à votre besoin):
```
cd /chemin/vers/dossier/de/travail/
```

Cloner votre projet vers le local :
```
git clone (Lien ssh ici)
# par exemple :
#git clone git@gitlab.com:..............git
```

Un dossier au nom de votre projet est créé.
Compléter le avec vos fichiers.

```
cd dossierProjetGitlab

```

Le fichier README.md est à l'intérieur (si l'option readme était coché à la création du projet).


## 5 bis) Autre méthode lorsque vous avez déjà un projet en local :
(Extrait de la doc gitlab)

Push an existing folder
```sh
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:.............git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## Vous pouvez me faire des retours

Si vous avez des difficultés pour la mise en oeuvre, si ma procédure comporte des erreurs, n'hésitez pas à me faire des retours.

15/12/2023
Bonjour